# Git & GitHub - Partie 2 Activité

## Objectifs

Cette activité permet de prendre en main les différentes fonctionnalités de Git. 
Cela nous permet de créer des "sauvegardes" de nos fichiers/dossiers dans le temps.

## Tâches à réaliser

Créer un fichier "README.txt" , dans mon cas, j'ai préféré un "README.md"
Créer également deux autres fichiers
Faire un minimum de 4 commits.

